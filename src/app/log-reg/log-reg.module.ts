import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTabsModule,MatButtonModule,
  MatButtonToggleModule,MatFormFieldModule,MatInputModule,MatIconModule,MatCardModule,
  MatDatepickerModule,MatNativeDateModule, MatSelectModule, MatCheckboxModule
} from '@angular/material';
import { LogRegRoutingModule } from './log-reg-routing.module';
import { LoginComponent } from './login/login.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {RegisterservService } from './registerserv.service'
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './token-interceptor.service';

@NgModule({
  declarations: [
    LoginComponent
  ],
  
  imports: [
    CommonModule,
    LogRegRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  providers: [RegisterservService,AuthGuard,
  {provide:HTTP_INTERCEPTORS,
useClass:TokenInterceptorService,
multi:true}],
  exports:[LoginComponent]
})
export class LogRegModule { }
