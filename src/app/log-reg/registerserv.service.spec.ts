import { TestBed } from '@angular/core/testing';

import { RegisterservService } from './registerserv.service';

describe('RegisterservService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterservService = TestBed.get(RegisterservService);
    expect(service).toBeTruthy();
  });
});
