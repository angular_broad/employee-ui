import { Injectable } from '@angular/core';
import { CanActivate,Router} from '@angular/router';
import {RegisterservService } from './registerserv.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{
 
 constructor(private router: Router, private reg: RegisterservService ){}
 
canActivate(): boolean{
  if (this.reg.loggedIn()){
    return true
  }else{
    this.router.navigate[('/edahboard')]
    return false
}
}
}


