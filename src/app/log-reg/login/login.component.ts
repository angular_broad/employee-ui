import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, } from '@angular/forms'
import {RegisterservService } from '../registerserv.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide=true;
  loginForm: FormGroup;
  registerForm: FormGroup;

  registerUserData={}
  loginUserData={}

  constructor(private router: Router,private fb:FormBuilder, private reg: RegisterservService) { 
    
    this.loginForm= new FormGroup({
      p_first_name:new FormControl(),
      p_password:new FormControl()
    });


    this.registerForm= new FormGroup({
      p_first_name:new FormControl(),
      p_middle_name:new FormControl(),
      p_last_name:new FormControl(),
      p_email_id:new FormControl(), 
      p_current_contact_no:new FormControl(),
      p_dob:new FormControl(),
      p_gender:new FormControl(),
      p_total_experience:new FormControl(),
      p_reference_1:new FormControl(),
      p_qualification_type:new FormControl(),
      p_per_of_marks_CGPA:new FormControl(),
      p_year_of_passing:new FormControl(),
      p_institute_name:new FormControl(),
    });

    this.createRegisterForm();
    this.createLoginForm();
  }





  createLoginForm(){
    this.loginForm = this.fb.group({
      p_first_name:['',Validators.required],
      p_password:['',Validators.required],
      });
  }

  createRegisterForm(){
    this.registerForm = this.fb.group({
      p_first_name:['',Validators.required],
      p_middle_name:['',Validators.required],
      p_last_name:['',Validators.required],
      p_email_id:['',Validators.required], 
      p_current_contact_no:['',Validators.required],
      p_dob:['',Validators.required],
      p_gender:['',Validators.required],
      p_total_experience:['',Validators.required],
      p_reference_1:[''],
      p_qualification_type:['',Validators.required],
      p_per_of_marks_CGPA:['',Validators.required],
      p_year_of_passing:['',Validators.required],
      p_institute_name:['',Validators.required],
    });
  };


  // public onLoginClick(){
  //   this.router.navigate(['edashboard/navigation'])
  // };

  ngOnInit() {
  }
registerUser() {
 this.reg.registerUser(this.registerUserData)
 .subscribe(
   res=>{console.log(res)
    localStorage.setItem('token',res.token)
    this.router.navigate(['/edashboard']) 
  },
   _err=>console.log(Error)
 )
}
loginUser() {
  // console.log(this.loginUserData)
  this.reg.registerUser(this.registerUserData)
 .subscribe(
  res=>{
    console.log(res)
  localStorage.setItem('token',res.token)
  this.router.navigate(['/edashboard'])
},
   _err=>console.log(Error)
 )
 }
}
