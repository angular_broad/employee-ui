import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {path: 'login', 
  redirectTo:'',
  pathMatch:'full' },

  {
    path: 'Login',
    loadChildren: './log-reg/log-reg.module#LogRegModule'

  },
  {
    path: 'edashboard',
    loadChildren: './edashboard/edashboard.module#EdashboardModule'

  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
