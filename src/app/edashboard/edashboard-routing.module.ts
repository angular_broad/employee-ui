import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { FormsComponent } from './forms/forms.component';
import { ProfileComponent } from './profile/profile.component';
import { FamilyComponent } from './family/family.component';
import { QualificationComponent } from './qualification/qualification.component';
import { PreviousComponent } from './previous/previous.component';

const routes: Routes = [
  {path:'',
  component:NavigationComponent,
  children:
  [ {path:'', component:ProfileComponent, pathMatch: 'full'},
    {path:'Forms', component:FormsComponent,pathMatch: 'full'},
    {path:'Family', component:FamilyComponent,pathMatch: 'full'},
    {path:'Qualification', component:QualificationComponent,pathMatch: 'full'},
    {path:'Previous', component:PreviousComponent,pathMatch: 'full'},
    {path:'Profile', component:ProfileComponent,pathMatch: 'full'},
  ],
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EdashboardRoutingModule { }
