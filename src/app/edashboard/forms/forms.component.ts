import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

 //   onSelectFile(event) {
//     if (event.target.files && event.target.files[0]) {
//       var reader = new FileReader();

//       reader.readAsDataURL(event.target.files[0]); // read file as data url
//  console.log(event);
//       // reader.onload = (event) => { // called once readAsDataURL is completed
//       //   this.url = event.target.result;
//       // }
//     }
//   }

firstFormGroup: FormGroup;
secondFormGroup: FormGroup;

constructor(private _formBuilder: FormBuilder) {}

ngOnInit() {
  this.firstFormGroup = this._formBuilder.group({
    p_first_name:['',Validators.required],
    p_middle_name:['',Validators.required],
    p_last_name:['',Validators.required],
    p_current_contact_no:['',Validators.required],
    p_email_id:['',Validators.required],
    p_permenant_address:['',Validators.required],
    p_present_address:['',Validators.required],
    p_dob:['',Validators.required],
    p_doj:['',Validators.required],
    p_gender:['',Validators.required],
    p_position_applied_by:['',Validators.required],
    p_expected_salary:['',Validators.required],
    p_strength:['',Validators.required],
    p_weeknesses:['',Validators.required],
    p_expectation_from_company:['',Validators.required],
    p_joining_notice_period:['',Validators.required],
    p_releving_letter_URL:['',Validators.required],
    p_total_experience:['',Validators.required],
    p_additional_info:['',Validators.required],
    p_location_preferenced_1:['',Validators.required],
    p_location_preferenced_2:['',Validators.required],
  });

}
}