import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EdashboardRoutingModule } from './edashboard-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule,
  MatStepperModule, 
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatCardModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule, } from '@angular/material';
  import { MatFileUploadModule } from 'angular-material-fileupload';
import { MatListModule } from '@angular/material/list';
import { FormsComponent } from './forms/forms.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { FamilyComponent } from './family/family.component';
import { QualificationComponent } from './qualification/qualification.component';
import { PreviousComponent } from './previous/previous.component';

@NgModule({
  declarations: [NavigationComponent, FormsComponent, ProfileComponent, FamilyComponent, QualificationComponent, PreviousComponent],
  imports: [
    CommonModule,
    EdashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatStepperModule,
    MatCheckboxModule,
    MatFileUploadModule
    
  ],

  exports:[
    NavigationComponent,FormsComponent,ProfileComponent
  ]
})
export class EdashboardModule { }
